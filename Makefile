include /etc/os-release

ARCHIVE_PREFIX = /srv/webapp/
SOURCE_ARCHIVE = fandango.tar

IMAGE_REPO = registry.gitlab.com/modioab/fandango/$(ID)-$(VERSION_ID)
IMAGE_FILES += $(FEDORA_ROOT_ARCHIVE)
IMAGE_FILES += $(SOURCE_ARCHIVE)

FEDORA_ROOT_ARCHIVE = rootfs.tar
FEDORA_ROOT_PACKAGES += python3
FEDORA_ROOT_PACKAGES += ImageMagick-libs
FEDORA_ROOT_PACKAGES += python3-pip
FEDORA_ROOT_PACKAGES += dejavu-sans-mono-fonts

include build.mk
