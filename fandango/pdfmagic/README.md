PDF magic
===============================

PDF magic
---------------

Python package for transforming PDFs to images, developed as part of a larger
PDF handling program.


Milestones:
---------------

- reached

    *  [x] Package for 600 dpi image generation from PDFs

- yet to be reached

    *  [ ] Image append: program that takes an empty PDF and inserts images into it
    *  [ ] Implement package into Pyramid deform project - text input frontend


Requirements:
---------------

- wand
    a MagicWand API binding for python, for a boatload of image tools
        
     install: 
    
    *  `apt-get install libmagickwand-dev`
    *  `pip install Wand`

    
Instructions:
---------------

- cd into `pdfmagic`

- Create a Python virtual environment.

    `python3 -m venv env`


- Activate virtual environment

    `source env/bin/activate`

- run `./pdftopng.py <input> <output>` where input is an existing pdf file including file extension (i.e `documents/test.pdf`) and output is the filepath to the output .png (i.e `images/myimage.png`). The .png(s) will always get a 0 indexed page number at the end of name.
