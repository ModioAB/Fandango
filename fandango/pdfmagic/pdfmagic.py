#! /usr/bin/env python

import logging
import time
from wand.image import Image
from wand.color import Color

log = logging.getLogger(__name__)


def pdftopng(pdfname):
    """Generates png objects from .PDF"""
    start = time.process_time()
    with Image(filename=pdfname, resolution=300) as orig:
        images = orig.sequence
        for image in images:
            page = Image(image=image).convert('png')
            page.background_color = Color('white')
            page.alpha_channel = 'remove'
            yield page
    spent = time.process_time() - start
    log.debug('Generating PNGs from %s, took %s sec', pdfname, spent)
