#! /usr/bin/env python
import logging
import datetime
import time
import tempfile
import os
from pyramid.response import FileResponse

log = logging.getLogger(__name__)


def mangler(pages):
    start = time.process_time()
    ts = datetime.datetime.now().timestamp()
    with tempfile.NamedTemporaryFile(
        'w+b', prefix='Generated_{!s}_'.format(ts),
        suffix='.pdf', delete=True
    ) as f:
        for p, page in enumerate(pages):
            if p == 0:
                converted = page.convert('pdf')
            else:
                converted.sequence.append(page)
        converted.save(filename=f.name)
        spent = time.process_time() - start
        size = os.stat(f.name).st_size
        p = p + 1 if p is not None else 0  # 0-indexed pagecount to 1-indexed
        log.info(
            'Created PDF: %s, pages: %s, size: %s bytes, spent: %s sec ',
            f.name, p, size, spent
        )
        response = FileResponse(
            f.name,
            content_type='application/pdf'
        )
        return response
