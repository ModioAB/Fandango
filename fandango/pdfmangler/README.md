PDF Mangler
===============================

PDF Mangler
---------------

Python program for generating PDFs from page specific PNGs,
developed as part of a more competent PDF handling program.



Milestones:
---------------

- reached
    *  [x] Package for 600 dpi image generation from PDFs
    *  [x] Image append: program that takes an empty PDF and inserts images into it

- yet to be reached

    *  [ ] Implement package into Pyramid deform project - text input frontend


Requirements:
---------------

- Wand
    
    install:
        `pip install Wand`

    
Instructions:
---------------

- cd into `pdf-mangler`

- Create a virtual environment
    `python3 -m venv env`

- Activate virtual environment
    `source env/bin/activate`

- Run the script
    `./pdf-mangler.py <page> <page> ... --output <pdfname>`
    * Where `<page>` is the relative filepath to a page in .png format, and `<pdfname>` is
      the desired relative filepath to the resulting pdf.
    * Pages are ordered as they are inputed
    
