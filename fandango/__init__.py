# fandango.py - run script for python application handling PDFs
import os
import json

from pyramid.config import Configurator
from pyramid.session import SignedCookieSessionFactory

from . import views


def get_prefill():
    # We can manage without a secret config...
    prefill = os.environ.get("PREFILL", '{}')
    # ... but we won't accept invalid json if we have one!
    bonus_map = json.loads(prefill)
    return bonus_map


def main(global_config, **settings):
    my_session_factory = SignedCookieSessionFactory('itsaseekreet')
    config = Configurator(settings=settings,
                          session_factory=my_session_factory)
    config.registry['prefill'] = get_prefill()
    config.include('pyramid_jinja2')
    config.add_route('main_view', '/')
    config.add_route('form_view', '{pdf_id}/form')
    config.add_route('preview', '{pdf_id}/preview')
    # config.add_route('form_view_json', '/form.json')
    config.add_static_view('deform_static', 'deform:static/')
    # config.scan('views')
    config.scan()
    views.check_files()
    return config.make_wsgi_app()
