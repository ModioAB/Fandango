import logging
import json
import pathlib
import textwrap

import colander
import deform
import peppercorn

from collections import defaultdict
from pyramid.httpexceptions import (
    HTTPFound,
)
from pyramid.response import FileResponse
from fandango.pdfinsert import (
    pdf_editor,
)

from pyramid.view import (
    view_config,
)

log = logging.getLogger(__name__)


PDFs = {
    '1': {
        'name': 'anmalan-om-certifikatkonto.pdf',
        'ppath': 'fandango/pdfs/anmalan-om-certifikatkonto.pdf',
        'cpath': 'fandango/configs/anmalan-om-certifikatkonto.json',
        'pages': 3,
    },
    '2': {
        'name': 'ansokanomelcertifikat.pdf',
        'ppath': 'fandango/pdfs/ansokanomelcertifikat.pdf',
        'cpath': 'fandango/configs/ansokanomelcertifikat.json',
        'pages': 12,
    },
    '3': {
        'name': 'elcert_avtal.pdf',
        'ppath': 'fandango/pdfs/elcert_avtal.pdf',
        'cpath': 'fandango/configs/elcert_avtal.json',
        'pages': 2,
    },
    '4': {
        'name': 'fullmakt.pdf',
        'ppath': 'fandango/pdfs/fullmakt.pdf',
        'cpath': 'fandango/configs/fullmakt.json',
        'pages': 2,
    },
}


def check_files():
    """Checks the files in our internal configuration"""

    def read_test(fname):
        """ Tries to read a byte from the file to see that we can access it"""
        p = pathlib.Path(fname)
        with p.open(mode='br') as f:
            try:
                size = p.stat().st_size
                f.read(1)
            except Exception:
                log.error("Failed to use: %s, size: %s", fname, size)
            else:
                log.debug("File ok: %s, size: %s", fname, size)

    for files in PDFs.values():
        read_test(files["ppath"])
        read_test(files["cpath"])


def wrap(text, width=75):
    '''textwrapper ignores newlines coming from the user, so, we break it down
    into user-specified lines before length-based wrapping'''
    wrapper = textwrap.TextWrapper(width=width)
    wrapped_values = []
    for line in text.splitlines():  # split on newlines
        for val in wrapper.wrap(line):  # split line on space bef. max width
            wrapped_values.append(val)
    return wrapped_values


def radio_operation(templine, value):
    page = templine['choices'][value]['page'] - 1
    operation = {
        'text': 'x',
        'page': page,
        'x': templine['choices'][value]['x'],
        'y': templine['choices'][value]['y']
    }
    yield operation


def textarea_operation(templine, value):
    consts = templine['start']
    width = consts['width']
    wrapped_values = wrap(value, width)
    if len(wrapped_values) > len(templine['lines']):
        raise Exception("Too many lines in operation")
    for i, val in enumerate(wrapped_values):
        page = consts['page'] - 1
        operation = {
            'text': val,
            'page': page,
            'x': consts['x'],
            'y': templine['lines'][i]['y'],
        }
        yield operation


def date_operation(templine, value):
    positions = templine['positions']
    for pos in positions:
        page = pos['page'] - 1
        operation = {
            'text': str(value),
            'page': page,
            'x': pos['x'],
            'y': pos['y'],
        }
        yield operation


def text_operation(templine, value):
    positions = templine['positions']
    for pos in positions:
        page = pos['page'] - 1
        operation = {
            'text': value,
            'page': page,
            'x': pos['x'],
            'y': pos['y'],
        }
        yield operation


def bonus_ops(request, operations, pdf_id):
    '''adding PDF-specific hidden personal info,
    as to not expose sensitive information in configs'''
    bonus_map = request.registry['prefill']
    bonus = bonus_map.get(pdf_id, [])
    for line in bonus:
        try:
            positions = line['positions']
            for pos in positions:
                page = pos['page'] - 1
                operation = {
                    'text': line['text'],
                    'page': page,
                    'x': pos['x'],
                    'y': pos['y'],
                }
                operations[page].append(operation)
        except KeyError:
            log.error('Malformed PREFILL config, no id called %s', pdf_id)
    return operations


def ops_builder(request, appstruct, my_template, pdf_id):
    ret = defaultdict(list)
    for x in operation_builder(appstruct, my_template):
        ret[x['page']].append(x)
    ret = bonus_ops(request, ret, pdf_id)
    return ret


def operation_builder(appstruct, my_template):
    ops_map = {
        "radio": radio_operation,
        "textarea": textarea_operation,
        "date": date_operation,
        "text": text_operation,
    }
    for key, value in appstruct.items():
        if not value:
            raise ValueError()
        template_line = {}
        for line in my_template:
            if line['text'] == key:
                template_line = line
        if not template_line:
            continue
        oper_extract = ops_map[template_line["type"]]
        yield from oper_extract(template_line, value)


def pdf_template(pdf_id):
    page_max = PDFs[pdf_id]['pages']
    config_path = PDFs[pdf_id]['cpath']
    log.debug(
        'Generating PDF template, id %s has %s pages, config_path: %s',
        pdf_id, page_max, config_path
    )
    pages = []
    for page in range(1, page_max + 1):
        pages += [(page, 'Page {}'.format(page))]
    with open(config_path) as json_data:
        configuration = json.load(json_data)
    return configuration


def pdfschema(template):
    schema = colander.Schema()
    log.debug('Generating schema from template with %s nodes', len(template))
    choices = (('ja', 'Ja'), ('nej', 'Nej'), ('vet ej', 'Vet ej'))
    for field in template:
        typ = ''
        title = ''
        try:
            typ = field['type']
            title = field['text']
        except KeyError:
            log.error('field %s is missing type or text, fix your configs!', field)
            raise
        if field['required']:
            requirement = colander.required
        else:
            requirement = colander.drop
        if typ == 'radio':
            radiochoices = choices[:len(field['choices'])]  # some radio widgets don't contain "vet ej"
            text = colander.SchemaNode(
                colander.String(),
                title=title,
                name=title,
                missing=requirement,
                widget=deform.widget.RadioChoiceWidget(values=radiochoices)
            )
            schema.add(text)
        elif typ == 'date':
            text = colander.SchemaNode(
                colander.Date(),
                title=title,
                name=title,
                missing=requirement,
                description='NOTE: Always inserts date in yyyy-mm-dd format',
                widget=deform.widget.DateInputWidget()
            )
            schema.add(text)
        elif typ == 'textarea':
            width = field['start']['width']
            height = len(field['lines'])
            text = colander.SchemaNode(
                colander.String(),
                title=title,
                name=title,
                missing=requirement,
                widget=deform.widget.TextAreaWidget(cols=width, rows=height)
            )
            schema.add(text)
        elif typ == 'text':
            text = colander.SchemaNode(
                colander.String(),
                title=title,
                name=title,
                missing=requirement,
                validator=colander.Length(1, 118),
            )
            schema.add(text)
        else:
            raise ValueError("Unsupported or missing type in template: %s" % field)
    return schema


class FandangoViews(object):
    def __init__(self, request):
        self.request = request

    def post_parser(self, schema):
        """Takes a POSTed form and validates input, generates an appstruct"""
        controls = self.request.POST.items()
        pstruct = peppercorn.parse(self.request.POST.items())
        schema = schema.bind(**pstruct)
        form = deform.Form(schema, buttons=("Create", ))
        return form.validate(controls)

    def response_builder(self, pdf_id, operations):
        pdf_path = PDFs[pdf_id]['ppath']
        if operations:
            return pdf_editor(pdf_path, operations)
        else:
            # No field filled, no need to regenerate the unaltered pdf
            log.info('No operations found, showing unaltered PDF...')
            location = self.request.route_path(
                'preview', pdf_id=pdf_id
            )
            return HTTPFound(location=location)

    # main view, basic controls..?
    @view_config(route_name='main_view', renderer='templates/main_view.jinja2')
    def main_view(self):
        return {'page_title': 'Main View'}

    @view_config(route_name='preview', renderer='string')
    def preview(self):
        pdf_id = self.request.matchdict['pdf_id']
        pdf_path = PDFs[pdf_id]['ppath']

        response = FileResponse(
            pdf_path,
            request=self.request,
            content_type='application/pdf',
        )
        log.debug('Showing unaltered pdf: {}'.format(pdf_path))
        return response

    @view_config(route_name='form_view', renderer='templates/form_view.jinja2')
    def form_view(self):
        # fetch which pdf we're editing from the URL
        pdf_id = self.request.matchdict['pdf_id']
        # getting template for schema / posted form
        my_template = pdf_template(pdf_id)
        schema = pdfschema(my_template)
        # generate form
        form = deform.Form(schema, buttons=("Create", ))
        log.info('Form with id {} generated'.format(pdf_id))

        if self.request.method == 'POST':
            if 'Create' not in self.request.params:
                self.request.session.flash('missing correct button')
            else:
                # map input to form
                try:
                    appstruct = self.post_parser(schema)
                except deform.ValidationFailure as e:
                    log.info('Validation failure')
                    return{"form": e.render()}
                # create instructions for pdfeditor()
                operations = ops_builder(self.request, appstruct, my_template, pdf_id)
                # generate appropriate response
                return self.response_builder(pdf_id, operations)

        return {
            'project': 'stuff',
            'form': form.render(),
            'reqts': form.get_widget_resources(),
            'pdf_name': PDFs[pdf_id]['name'],
        }
