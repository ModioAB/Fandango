#! /usr/bin/env python

from wand.color import Color
from wand.drawing import Drawing


def pixelate_snake(centimeter):
    """Converts user inputed centimeters to pixels for the text element"""
    # 1 inch = 2.54 cm
    dpi = 300  # dpi of image, change if needed!
    pixels = int(round(centimeter * (dpi / 2.54)))
    return pixels


def adder(image, operations, size=47):
    """Create a new .png by adding text to an existing image."""
    with Drawing() as context:
        # Future support for fonts?
        context.fill_color = Color('black')
        context.font_size = size
        context.font = '/usr/share/fonts/dejavu/DejaVuSansMono.ttf'
        for t in operations:
            txt = t['text']
            x = pixelate_snake(t['x'])
            y = pixelate_snake(t['y'])
            context.text(x=x, y=y, body=txt)
        context(image)
        return context
