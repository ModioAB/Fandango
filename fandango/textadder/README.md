Text-adder
===============================

Text Adder: a pretty literate viper
---------------

Python program for adding text elements to images, as part of a larger 
program developed to handle .PDFs.



Milestones:
---------------

- reached
    *  [x] Package for 600 dpi image generation from PDFs
    *  [x] Image append: program that takes an empty PDF and inserts images into it
    *  [x] Text adder: program that adds texts to .PNGs

- yet to be reached

    *  [ ] Implement package into Pyramid deform project - text input frontend


Requirements:
---------------

- Wand
    
    install: `pip install wand`
    
Instructions:
---------------

- cd into `text-adder`

- Create a virtual environment
    `python3 -m venv env`

- Activate virtual environment
    `source env/bin/activate`

- Run the script
    `./text-adder.py <image> <text> -x <x-coordinate> -y <y-coordinate> -s <fontsize>
    * Where `<image>` is the relative filepath to an image (assumed to be 600dpi)
    * `<text>` is the desired text to be inserted
    * `<x-coordinate>` and `<y-coordinate>` determine the position of the text box, in centimeters
    relative to the top left corner of the image, and bottom left of the text
    * `<fontsize>` is the fontsize in px, and the only optional flag/option
    * the script WRITES OVER the input png for compatibility reasons,  
    if something went wrong, regenerate your page-specific pngs with pdfmagic
