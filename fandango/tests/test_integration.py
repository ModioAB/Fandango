import json
from fandango.views import PDFs


def test_json_live_config():
    for index in PDFs.values():
        fname = index['cpath']
        with open(fname) as f:
            json.load(f)
