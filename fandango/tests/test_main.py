import unittest
import colander
import datetime

from collections import defaultdict
from pyramid import testing

from fandango.views import (
    FandangoViews,
    ops_builder,
    operation_builder,
    pdfschema,
    pdf_template,
)

from webob.multidict import MultiDict
from pyramid.response import FileResponse


def dummy_request():
    request = testing.DummyRequest()
    request.POST = MultiDict()
    return request


class HTMLTests(unittest.TestCase):
    def setUp(self):
        from fandango import main
        app = main({})
        from webtest import TestApp

        self.testapp = TestApp(app)

    def tearDown(self):
        pass

    # Only looks at html contents, in wrong class because of this?
    def test_main_view_contents(self):
        res = self.testapp.get('/', status=200)
        self.assertIn(b'Main', res.body)

    def test_form_contents(self):
        """Tests that the correct form is displayed"""
        res = self.testapp.get('/1/form', status=200)
        self.assertIn(b'anmalan-om-certifikatkonto.pdf', res.body)
        self.assertIn('(Konto: Företagsnamn/namn)', res.text)

        res = self.testapp.get('/2/form', status=200)
        self.assertIn(b'ansokanomelcertifikat.pdf', res.body)
        self.assertIn('(2 Sökande: namn/företag)', res.text)

        res = self.testapp.get('/3/form', status=200)
        self.assertIn(b'elcert_avtal.pdf', res.body)
        self.assertIn('(Adress: Namn)', res.text)

        res = self.testapp.get('/4/form', status=200)
        self.assertIn(b'fullmakt.pdf', res.body)
        self.assertIn('(Fullmaktsgivarens företagsnamn/namn)', res.text)


class FunctionalTests(unittest.TestCase):
    def setUp(self):
        self.pdf_id = '4'
        self.pdf_name = 'fullmakt.pdf'
        self.pdf_path = 'pdfs/fullmakt.pdf'
        self.appstruct = {
            '(Fullmaktsgivarens företagsnamn/namn)': 'a',
            '(org-/personnummer)': 'b',
            '(E-postadress kontaktperson fullmaktsgivare)': 'c',
            '(Namnförtydligande)': 'e',
        }
        self.my_template = [
            {
                'text': '(Fullmaktsgivarens företagsnamn/namn)',
                'required': True,
                'type': 'text',
                'positions': [
                    {'x': 3.7, 'y': 19.56, 'page': 1}
                ]
            },
            {
                'text': '(org-/personnummer)',
                'required': True,
                'type': 'text',
                'positions': [
                    {'x': 13.2, 'y': 19.56, 'page': 1}
                ]
            },
            {
                'text': '(E-postadress kontaktperson fullmaktsgivare)',
                'required': True,
                'type': 'text',
                'positions': [
                    {'x': 3.7, 'y': 20.43, 'page': 1}
                ]
            },
            {
                'text': '(Ort och Datum)',
                'required': False,
                'type': 'text',
                'positions': [
                    {'x': 3.7, 'y': 22.97, 'page': 1}
                ]
            },
            {
                'text': '(Namnförtydligande)',
                'required': True,
                'type': 'text',
                'positions': [
                    {'x': 10.65, 'y': 22.97, 'page': 1}
                ]
            },
        ]
        self.request = dummy_request()
        self.request.registry['prefill'] = {
            '1': [
                {
                    'text': 'Kont.Ansvarig: Namn',
                    'positions': [
                        {'x': 2.19, 'y': 18.0, 'page': 1}
                    ]
                },
                {
                    'text': 'Kont.Ansvarig: Personnummer',
                    'positions': [
                        {'x': 15.55, 'y': 18.0, 'page': 1}
                    ]
                },
                {
                    'text': 'Kont.Ansvarig: Adress',
                    'positions': [
                        {'x': 12.55, 'y': 18.78, 'page': 1}
                    ]
                },
                {
                    'text': 'Kont.Ansvarig: Postnummer',
                    'positions': [
                        {'x': 2.19, 'y': 19.53, 'page': 1}
                    ]
                },
                {
                    'text': 'Kont.Ansvarig: Ort',
                    'positions': [
                        {'x': 7.4, 'y': 19.53, 'page': 1}
                    ]
                },
                {
                    'text': 'Kont.Ansvarig: Telefonnummer',
                    'positions': [
                        {'x': 2.19, 'y': 20.27, 'page': 1}
                    ]
                },
                {
                    'text': 'Kont.Ansvarig: E-postadress',
                    'positions': [
                        {'x': 7.4, 'y': 20.27, 'page': 1}
                    ]
                }
            ],
            '2': [
                {
                    'text': 'Rapp.Företag: Namn kont',
                    'positions': [
                        {'x': 2.19, 'y': 5.21, 'page': 2}
                    ]
                },
                {
                    'text': 'Rapp.Företag: Tfn kont',
                    'positions': [
                        {'x': 15.5, 'y': 5.21, 'page': 2}
                    ]
                },
                {
                    'text': 'Rapp.Företag: E-postadress kont',
                    'positions': [
                        {'x': 2.19, 'y': 5.92, 'page': 2}
                    ]
                },
                {
                    'text': 'kontaktperson namn',
                    'positions': [
                        {'x': 2.19, 'y': 10.55, 'page': 4},
                        {'x': 2.19, 'y': 17.1, 'page': 1}
                    ]
                },
                {
                    'text': 'kontaktperson tfn',
                    'positions': [
                        {'x': 2.19, 'y': 11.38, 'page': 4},
                        {'x': 2.19, 'y': 17.97, 'page': 1}
                    ]
                },
                {
                    'text': 'kontaktperson mobilnummer',
                    'positions': [
                        {'x': 6.68, 'y': 11.38, 'page': 4}
                    ]
                },
                {
                    'text': 'kontaktperson e-post',
                    'positions': [
                        {'x': 11.17, 'y': 11.38, 'page': 4},
                        {'x': 7.4, 'y': 17.97, 'page': 1}
                    ]
                }
            ],
            '3': [],
            '4': [
                {
                    'text': 'E-postadress kontaktperson fullmakt',
                    'positions': [
                        {'page': 1, 'x': 3.7, 'y': 7.3}
                    ]
                }
            ]
        }

        self.operations = ops_builder(self.request, self.appstruct,
                                      self.my_template, self.pdf_id)
        self.view = FandangoViews(self.request)

    # post_parser tests
    def test_post_parser_filled_form_returns_appstruct(self):
        schema = pdfschema(self.my_template)
        self.request.POST.add('(Fullmaktsgivarens företagsnamn/namn)', 'a')
        self.request.POST.add('(org-/personnummer)', 'b')
        self.request.POST.add('(E-postadress kontaktperson fullmaktsgivare)', 'c')
        self.request.POST.add('(Ort och Datum)', '')
        self.request.POST.add('(Namnförtydligande)', 'e')
        self.request.POST.add('Create', 'create')
        self.request.method = 'POST'
        appstruct = self.view.post_parser(schema)
        expected_appstruct = self.appstruct
        self.assertEqual(appstruct, expected_appstruct)

    def test_post_parser_bad_button_is_removed(self):
        """tests that unregistered stuff in the POST.items are thrown away"""
        # We AREN'T catching malformed buttons or fields in post_parser.
        # We just throw them away at validation
        schema = pdfschema(self.my_template)
        self.request.POST.add('(Fullmaktsgivarens företagsnamn/namn)', 'scriptkiddie')
        self.request.POST.add('(org-/personnummer)', 'noneofyourbusiness')
        self.request.POST.add('(E-postadress kontaktperson fullmaktsgivare)', '1337haxxor@leet.se')
        self.request.POST.add('(Ort och Datum)', '')
        self.request.POST.add('(Namnförtydligande)', 'urk')
        self.request.POST.add('(Totally not an injected field)', 'woohooooo')  # bad field
        self.request.POST.add('Submit', 'submit')  # bad button!
        self.request.method = 'POST'
        appstruct = self.view.post_parser(schema)
        expected_appstruct = {
            '(Fullmaktsgivarens företagsnamn/namn)': 'scriptkiddie',
            '(org-/personnummer)': 'noneofyourbusiness',
            '(E-postadress kontaktperson fullmaktsgivare)': '1337haxxor@leet.se',
            '(Namnförtydligande)': 'urk',
        }  # no bad field, no bad button!
        self.assertEqual(appstruct, expected_appstruct)

    def test_post_parser_empty_form_should_explode_on_validation(self):
        pass

    # test ops_builder
    def test_ops_builder_works(self):
        """Tests that ops_builder returns a set of 'operations'"""
        operations = ops_builder(self.request, self.appstruct, self.my_template, self.pdf_id)
        # input value, 'a' from self.appstruct,
        # mapped to the position from self.my_template
        expected_ops = {
            0: [
                {'text': 'a', 'page': 0, 'x': 3.7, 'y': 19.56},
                {'text': 'b', 'page': 0, 'x': 13.2, 'y': 19.56},
                {'text': 'c', 'page': 0, 'x': 3.7, 'y': 20.43},
                {'text': 'e', 'page': 0, 'x': 10.65, 'y': 22.97},
                {'text': 'E-postadress kontaktperson fullmakt', 'page': 0, 'x': 3.7, 'y': 7.3},
            ]
        }
        self.assertEqual(operations, expected_ops)

    def test_ops_builder_multi_type_fields_should_work(self):
        pdf_id = '2'
        my_template = [
            {
                'text': '(2 Sökande: namn/företag)', 'required': True, 'type': 'text', 'positions': [
                    {'x': 2.19, 'y': 12.53, 'page': 1}, {'x': 2.19, 'y': 9.29, 'page': 4}
                ]
            },
            {
                'text': '(2 Sökande: org/personnummer)', 'required': False, 'type': 'text', 'positions': [
                    {'x': 11.07, 'y': 12.53, 'page': 1}, {'x': 11.18, 'y': 9.29, 'page': 4}
                ]
            },
            {
                'text': '(2 Sökande: Innehav %)', 'required': False, 'type': 'text', 'positions': [
                    {'x': 15.6, 'y': 12.53, 'page': 1}, {'x': 15.77, 'y': 9.29, 'page': 4}
                ]
            },
            # date input field
            {
                'text': '(3 Grunduppgifter: Datum då anläggningen var ny och först producerade el)',
                'required': False,
                'type': 'date',
                'positions': [
                    {'x': 2.19, 'y': 20.2, 'page': 1}
                ]
            },
            {
                'text': '(3 Grunduppgifter: Antal produktionsenheter bakom anläggningens mätpunkt)',
                'required': False,
                'type': 'text',
                'positions': [
                    {'x': 10.97, 'y': 20.2, 'page': 1}
                ]
            },
            # radio input field
            {
                "text": "(6 Medgivande)",
                "required": False,
                'type': 'radio',
                "choices": {
                    "ja": {
                        "x": 2.21,
                        "y": 9.18,
                        "page": 2
                    },
                    "nej": {
                        "x": 15.66,
                        "y": 9.18,
                        "page": 2
                    }
                }
            },
            {
                'text': '(UG 6: Namnförtydligande)',
                'required': False,
                'type': 'text',
                'positions': [
                    {'x': 2.19, 'y': 25.64, 'page': 4}
                ]
            },
            {
                "text": "(7 Övriga upplysningar)",
                "required": False,
                "type": "textarea",
                "start": {"x": 2.19, "page": 2, "width": 81},
                "lines": [
                    {"y": 10.56}, {"y": 11.06}, {"y": 11.53}, {"y": 12.05},
                    {"y": 12.56}, {"y": 13.10}, {"y": 13.58}, {"y": 14.07},
                    {"y": 14.57}
                ]
            }
        ]
        # appstructs values must match the type of the input, not the output!
        appstruct = {
            '(2 Sökande: namn/företag)': 'Nepp',
            '(3 Grunduppgifter: Datum då anläggningen var ny och först producerade el)': datetime.date(2018, 3, 13),
            '(6 Medgivande)': 'nej',
            '(7 Övriga upplysningar)': 'Did you know that the first Matrix was designed to be a perfect human world? Where none suffered, where everyone would be happy. It was a disaster. No one would accept the program. Entire crops were lost. Some believed we lacked the programming language to describe your perfect world. But I believe that, as a species, human beings define their reality through suffering and misery. The perfect world was a dream that your primitive cerebrum kept trying to wake up from. Which is why the Matrix was redesigned to this: the peak of your civilization.'
        }
        operations = ops_builder(self.request, appstruct, my_template, pdf_id)
        expected_ops = defaultdict(list, {
            0: [
                {'text': 'Nepp', 'page': 0, 'x': 2.19, 'y': 12.53},
                {'text': '2018-03-13', 'page': 0, 'x': 2.19, 'y': 20.2}
            ],
            1: [
                {
                    'text': 'x',
                    'page': '1',
                    'x': 2.21,
                    'y': 9.18
                },
                {
                    'text': 'Did you know that the first Matrix was designed to be a perfect human world?',
                    'page': '1',
                    'x': 2.19,
                    'y': 10.56
                },
                {
                    'text': 'Where none suffered, where everyone would be happy. It was a disaster. No one',
                    'page': '1',
                    'x': 2.19,
                    'y': 11.06
                },
                {
                    'text': 'would accept the program. Entire crops were lost. Some believed we lacked the',
                    'page': '1',
                    'x': 2.19,
                    'y': 11.53
                },
                {
                    'text': 'programming language to describe your perfect world. But I believe that, as a',
                    'page': '1',
                    'x': 2.19,
                    'y': 12.05
                },
                {
                    'text': 'species, human beings define their reality through suffering and misery. The',
                    'page': '1',
                    'x': 2.19,
                    'y': 12.56
                },
                {
                    'text': 'perfect world was a dream that your primitive cerebrum kept trying to wake up',
                    'page': '1',
                    'x': 2.19,
                    'y': 13.10
                },
                {
                    'text': 'from. Which is why the Matrix was redesigned to this: the peak of your',
                    'page': '1',
                    'x': 2.19,
                    'y': 13.58
                },
                {
                    'text': 'civilization.',
                    'page': '1',
                    'x': 2.19,
                    'y': 14.07
                }
            ],
            3: [
                {'text': 'Nepp', 'page': 3, 'x': 2.19, 'y': 9.29}
            ]
        })
        # We can't use assertEqual since the order of pages may be scrambled
        self.assertCountEqual(operations, expected_ops)

    def test_ops_builder_junk_in_appstruct_should_be_ignored(self):
        pdf_id = '2'
        my_template = [
            {
                'text': '(2 Sökande: namn/företag)',
                'required': True,
                'type': 'text',
                'positions': [
                    {'x': 2.19, 'y': 12.53, 'page': 1}, {'x': 2.19, 'y': 9.29, 'page': 4}
                ]
            },

            # date input field
            {
                'text': '(3 Grunduppgifter: Datum då anläggningen var ny och först producerade el)',
                'required': False,
                'type': 'date',
                'positions': [
                    {'x': 2.19, 'y': 20.2, 'page': 1}
                ]
            },
            # radio input field
            {
                'text': '(6 Medgivande)',
                'required': False,
                'type': 'radio',
                'choices': {
                    'ja': {
                        'x': 2.21,
                        'y': 9.18,
                        'page': 2
                    },
                    'nej': {
                        'x': 15.66,
                        'y': 9.18,
                        'page': 2
                    }
                }
            }
        ]
        # appstructs values must match the type of the input, not the output!
        appstruct = {
            '(2 Sökande: namn/företag)': 'Nepp',
            '(3 Grunduppgifter: Datum då anläggningen var ny och först producerade el)': datetime.date(2018, 3, 13),
            '(6 Medgivande)': 'nej',
            '(text jibberish)': 'blue',  # not speced in the template, ignored
            '(radio gaga)': 'ja',  # not specced, ignored
            '(date trash)': datetime.date(1995, 2, 28)  # not specced, ignored
        }
        operations = ops_builder(self.request, appstruct, my_template, pdf_id)
        expected_ops = defaultdict(list, {
            0: [
                {'text': 'Nepp', 'page': 0, 'x': 2.19, 'y': 12.53},
                {'text': '2018-03-13', 'page': 0, 'x': 2.19, 'y': 20.2}
            ],
            1: [
                {'text': 'x', 'page': '1', 'x': 15.66, 'y': 9.18}
            ],
            3: [
                {'text': 'Nepp', 'page': 3, 'x': 2.19, 'y': 9.29}
            ]
        })
        # We can't use assertEqual since the order of pages may be scrambled
        self.assertCountEqual(operations, expected_ops)

    def test_ops_builder_empty_form_should_return_empty_operations(self):
        """Tests that ops_builder returns an empty set of 'operations'
        when form is empty"""
        appstruct = {}
        pdf_id = '3'
        operations = ops_builder(self.request, appstruct, self.my_template, pdf_id)
        self.assertEqual(operations, {})

    def test_ops_builder_empty_form_with_prefill_should_return_prefill(self):
        """Tests that ops_builder returns a set of 'operations' only
        containing prefilled values"""
        appstruct = {}
        pdf_id = '4'
        operations = ops_builder(self.request, appstruct, self.my_template, pdf_id)
        expected_ops = defaultdict(list, {
            0: [
                {
                    'text': 'E-postadress kontaktperson fullmakt',
                    'page': 0, 'x': 3.7, 'y': 7.3
                }
            ]
        })
        self.assertEqual(operations, expected_ops)

    def test_ops_builder_no_template_should_generate_no_operations(self):
        """Considering the ability to post to a page without carrying any
        template"""
        my_template = []
        pdf_id = '3'
        operations = ops_builder(self.request, self.appstruct, my_template, pdf_id)
        self.assertEqual(operations, {})

    def test_ops_builder_bad_pdf_id(self):
        pdf_id = 'goat'
        ops = ops_builder(self.request, self.appstruct, self.my_template, pdf_id)
        expected_ops = defaultdict(list, {
            0: [
                {
                    'text': 'a',
                    'page': 0, 'x': 3.7, 'y': 19.56
                },
                {
                    'text': 'b',
                    'page': 0, 'x': 13.2, 'y': 19.56
                },
                {
                    'text': 'c',
                    'page': 0, 'x': 3.7, 'y': 20.43
                },
                {
                    'text': 'e',
                    'page': 0, 'x': 10.65, 'y': 22.97
                }
            ]
        })
        self.assertEqual(ops, expected_ops)

    # test operation_builder
    def test_operation_builder_should_work(self):
        operation = operation_builder(self.appstruct, self.my_template)
        expected = (
            {'page': 0, 'text': 'a', 'x': 3.7, 'y': 19.56},
            {'page': 0, 'text': 'b', 'x': 13.2, 'y': 19.56},
            {'page': 0, 'text': 'c', 'x': 3.7, 'y': 20.43},
            {'page': 0, 'text': 'e', 'x': 10.65, 'y': 22.97}
        )
        self.assertEqual(tuple(operation), expected)

    def test_operation_builder_no_template_should_generate_no_operation(self):
        my_template = []
        operation = operation_builder(self.appstruct, my_template)
        self.assertEqual(tuple(operation), ())

    def test_operation_builder_no_appstruct_should_return_nothing(self):
        appstruct = {}
        operation = operation_builder(appstruct, self.my_template)
        self.assertEqual(tuple(operation), ())

    def test_operation_builder_missing_value_in_appstruct_should_fail(self):
        appstruct = {
            '(Fullmaktsgivarens företagsnamn/namn)': 'a',
            '(org-/personnummer)': '',  # no value!
            '(E-postadress kontaktperson fullmaktsgivare)': 'c',
            '(Namnförtydligande)': 'e',
        }
        my_template = [
            {
                'text': '(Fullmaktsgivarens företagsnamn/namn)',
                'required': True,
                'type': 'text',
                'positions': [
                    {'x': 3.7, 'y': 19.56, 'page': 1}
                ]
            },
            {
                'text': '(org-/personnummer)',
                'required': True,
                'type': 'text',
                'positions': [
                    {'x': 13.2, 'y': 19.56, 'page': 1}
                ]
            },
            {
                'text': '(E-postadress kontaktperson fullmaktsgivare)',
                'required': True,
                'type': 'text',
                'positions': [
                    {'x': 3.7, 'y': 20.43, 'page': 1}
                ]
            },
            {
                'text': '(Ort och Datum)',
                'required': False,
                'type': 'text',
                'positions': [
                    {'x': 3.7, 'y': 22.97, 'page': 1}
                ]
            },
            {
                'text': '(Namnförtydligande)',
                'required': True,
                'type': 'text',
                'positions': [
                    {'x': 10.65, 'y': 22.97, 'page': 1}
                ]
            },
        ]
        with self.assertRaises(ValueError):
            list(operation_builder(appstruct, my_template))

    # test response_builder
    def test_response_builder_should_give_filled_fileresponse(self):
        """With a valid pdf_id and a set of operations, a modified file is
        returned as a FileResponse"""
        response = self.view.response_builder(self.pdf_id, self.operations)
        self.assertIsInstance(response, FileResponse)

    def test_response_builder_should_redirect_to_unfilled_fileresponse(self):
        """With a valid pdf_id but no operations, an unaltered file is returned
        as a FileResponse, through a redirect"""
        with testing.testConfig() as config:
            config.add_route('preview', '{pdf_id}/preview')
            response = self.view.response_builder(self.pdf_id, operations=None)
            self.assertEqual(response.location, '/4/preview')

    def test_response_builder_invalid_pdf_id_should_fail(self):
        """Can't edit a pdf that doesn't exist..."""
        pdf_id = '-3'
        with self.assertRaises(KeyError):
            self.view.response_builder(pdf_id, self.operations)

    def test_response_builder_invalid_pdf_id_and_empty_ops_should_fail(self):
        """... or display it for that matter"""
        pdf_id = 'fffff'
        with self.assertRaises(KeyError):
            self.view.response_builder(pdf_id, operations=None)

    # test pdf_template
    def test_pdf_template_valid_pdf_id_should_work(self):
        template = pdf_template(self.pdf_id)
        self.assertEqual(template, self.my_template)

    def test_pdf_template_invalid_pdf_id_should_fail(self):
        pdf_id = 90001
        with self.assertRaises(KeyError):
            pdf_template(pdf_id)

    def test_pdf_template_no_pdf_id_should_fail(self):
        pdf_id = ''
        with self.assertRaises(KeyError):
            pdf_template(pdf_id)

    # pdfschema tests
    def test_pdfschema(self):
        """pdfschema creates a schema object,
        check that it contains what it should.
        Generate the schema for a form containing all kinds of inputs"""
        # diverse test to catch all kinds of inputs
        # requires a template
        my_template = [
            {
                'text': '(2 Sökande: namn/företag)',
                'required': True,
                'type': 'text',
                'positions': [
                    {'x': 2.19, 'y': 12.53, 'page': 1}, {'x': 2.19, 'y': 9.29, 'page': 4}
                ]
            },
            {
                'text': '(2 Sökande: org/personnummer)',
                'required': False,
                'type': 'text',
                'positions': [
                    {'x': 11.07, 'y': 12.53, 'page': 1}, {'x': 11.18, 'y': 9.29, 'page': 4}
                ]
            },
            {
                'text': '(2 Sökande: Innehav %)',
                'required': False,
                'type': 'text',
                'positions': [
                    {'x': 15.6, 'y': 12.53, 'page': 1}, {'x': 15.77, 'y': 9.29, 'page': 4}
                ]
            },
            # date input field
            {
                'text': '(3 Grunduppgifter: Datum då anläggningen var ny och först producerade el)',
                'required': False,
                'type': 'date',
                'positions': [
                    {'x': 2.19, 'y': 20.2, 'page': 1}
                ]
            },
            {
                'text': '(3 Grunduppgifter: Antal produktionsenheter bakom anläggningens mätpunkt)',
                'required': False,
                'type': 'text',
                'positions': [
                    {'x': 10.97, 'y': 20.2, 'page': 1}
                ]
            },
            # checkbox input field
            {
                'text': '(6 Medgivande)',
                'required': False,
                'type': 'radio',
                'choices': {
                    'ja': {
                        'x': 2.21,
                        'y': 9.18,
                        'page': 2
                    },
                    'nej': {
                        'x': 15.66,
                        'y': 9.18,
                        'page': 2
                    }
                }
            },
            {
                'text': '(UG 6: Namnförtydligande)',
                'required': False,
                'type': 'text',
                'positions': [
                    {'x': 2.19, 'y': 25.64, 'page': 4}
                ]
            },
            # textarea input field
            {
                "text": "(7 Övriga upplysningar)",
                "required": False,
                "type": "textarea",
                "start": {"x": 2.19, "page": 2, "width": 81},
                "lines": [
                    {"y": 10.56}, {"y": 11.06}, {"y": 11.53}, {"y": 12.05},
                    {"y": 12.56}, {"y": 13.10}, {"y": 13.58}, {"y": 14.07},
                    {"y": 14.57}
                ]
            }
        ]
        schema = pdfschema(my_template)
        radio_node = schema['(6 Medgivande)']
        date_node = schema['(3 Grunduppgifter: Datum då anläggningen var ny och först producerade el)']
        string_node = schema['(2 Sökande: namn/företag)']
        rad1_node = schema['(7 Övriga upplysningar)']
        self.assertIsInstance(radio_node.typ, colander.String)
        self.assertIsInstance(date_node.typ, colander.Date)
        self.assertIsInstance(string_node.typ, colander.String)
        self.assertIsInstance(rad1_node.typ, colander.String)
        self.assertEqual(rad1_node.title, '(7 Övriga upplysningar)')
        schem_len = 0
        for node in schema:
            schem_len = schem_len + 1
            for item in my_template:
                if item['text'] == node.name:
                    template_item = item
            self.assertEqual(node.name, template_item['text'])
        temp_len = len(my_template)
        self.assertEqual(schem_len, temp_len)

    def test_pdfschema_bad_field(self):
        my_template = [
            {
                'text': '(2 Sökande: namn/företag)',
                'required': True,
                'type': 'text',
                'positions': [
                    {'x': 2.19, 'y': 12.53, 'page': 1}, {'x': 2.19, 'y': 9.29, 'page': 4}
                ]
            },
            {
                'text': '(2 Sökande: org/personnummer)',
                'required': False,
                'type': 'text',
                'positions': [
                    {'x': 11.07, 'y': 12.53, 'page': 1}, {'x': 11.18, 'y': 9.29, 'page': 4}
                ]
            },
            {
                'text': '(2 Sökande: Innehav %)',
                'required': False,
                'type': 'text',
                'positions': [
                    {'x': 15.6, 'y': 12.53, 'page': 1}, {'x': 15.77, 'y': 9.29, 'page': 4}
                ]
            },
            # date input field
            {
                'text': '(3 Grunduppgifter: Datum då anläggningen var ny och först producerade el)',
                'required': False,
                'type': 'date',
                'positions': [
                    {'x': 2.19, 'y': 20.2, 'page': 1}
                ]
            },
            {
                'text': '(3 Grunduppgifter: Antal produktionsenheter bakom anläggningens mätpunkt)',
                'required': False,
                'type': 'text',
                'positions': [
                    {'x': 10.97, 'y': 20.2, 'page': 1}
                ]
            },
            # checkbox input field
            {
                'text': '(6 Medgivande)',
                'required': False,
                'type': 'radio',
                'choices': {
                    'ja': {
                        'x': 2.21,
                        'y': 9.18,
                        'page': 2
                    },
                    'nej': {
                        'x': 15.66,
                        'y': 9.18,
                        'page': 2
                    }
                }
            },
            {
                'text': '(UG 6: Namnförtydligande)',
                'required': False,
                'type': 'text',
                'positions': [
                    {'x': 2.19, 'y': 25.64, 'page': 4}
                ]
            },
            {
                'text': '(7 Övriga upplysningar)',
                'required': False,
                'type': 'textarea',
                'start': {'x': 2432424.2, 'page': 2, 'width': 78},
                'lines': [
                    {"y": 10.56}, {"y": 11.06}, {"y": 11.53}, {"y": 12.05},
                    {"y": 12.56}, {"y": 13.10}, {"y": 13.58}, {"y": 14.07},
                    {"y": 14.57}
                ]
            },
            # type is not recognized, casting a ValueError
            {
                'text': '(Some bad field)',
                'required': False,
                'type': 'badtype',
                'positions': [
                    {'x': 2.19, 'y': 10.56, 'page': 2}
                ]
            }
        ]
        with self.assertRaises(ValueError):
            pdfschema(my_template)

    def test_pdfschema_empty_template(self):
        """pdfschema creates an empty schema object,
        check that it is indeed empty"""
        my_template = []
        schema = pdfschema(my_template)
        self.assertCountEqual(schema, [])  # schema has no elements

    def test_pdfschema_missing_parts_of_dict(self):
        my_template = [
            {
                'text': '(2 Sökande: namn/företag)',
                'required': True,
                'type': 'text',
                'positions': [
                    {'x': 2.19, 'y': 12.53, 'page': 1}, {'x': 2.19, 'y': 9.29, 'page': 4}
                ]
            },
            {
                'text': '(2 Sökande: org/personnummer)',
                'required': False,
                'positions': [
                    {'x': 11.07, 'y': 12.53, 'page': 1}, {'x': 11.18, 'y': 9.29, 'page': 4}
                ]
            }
        ]
        with self.assertRaises(KeyError):
            pdfschema(my_template)

    # preview tests
    def test_preview_valid_id(self):
        self.request.matchdict['pdf_id'] = '2'
        response = FandangoViews.preview(self)
        self.assertIsInstance(response, FileResponse)

    def test_preview_invalid_id(self):
        self.request.matchdict['pdf_id'] = '-2'
        with self.assertRaises(KeyError):
            FandangoViews.preview(self)
