import unittest
import json
import os

from fandango import get_prefill


class FunctionalTests(unittest.TestCase):
    def test_get_prefill_working(self):
        os.environ["PREFILL"] = '{"1": [], "2": [], "3": [], "4": []}'
        bonus_map = get_prefill()
        expected = {'1': [], '2': [], '3': [], '4': []}
        self.assertEqual(bonus_map, expected)

    def test_get_prefill_bad_json(self):
        os.environ["PREFILL"] = '{"1": [], "2": [], "3": [], "4": [],}'  # end comma is incorrect JSON
        with self.assertRaises(json.JSONDecodeError):
            get_prefill()

    def test_get_prefill_no_env_variable(self):
        os.environ.pop("PREFILL")
        bonus_map = get_prefill()
        self.assertEqual(bonus_map, {})
