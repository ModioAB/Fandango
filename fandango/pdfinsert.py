#! /usr/bin/env python
from fandango.textadder.textadder import adder
from fandango.pdfmagic.pdfmagic import (
    pdftopng,
)
from fandango.pdfmangler.pdfmangler import (
    mangler,
)


def pdf_editor(input_filename, operations):
    """applies operations to pdf, returns FileResponse object from mangler"""
    pages = pdftopng(input_filename)

    def internal(pages):
        for p, page in enumerate(pages):
            operations_to_apply = operations[p]
            adder(page, operations_to_apply)
            yield page

    pagelist = internal(pages)
    return mangler(pagelist)
