import sys

from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand

requires = [
    'pyramid',
    'pyramid_jinja2',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'deform',
    'pytest',
    'webtest',
    'waitress',
    'pytest-cov',
    'wand',
]

setup_requires = [
    'flake8',
    'wand',
]

tests_require = [
    'flake8',
    'WebTest >= 1.3.1',  # py3 compat
    'pytest',
    'pytest-cov',
    'wand',
]


class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass to py.test")]

    def initialize_options(self):
        super().initialize_options()
        self.pytest_args = ['--cov=fandango']

    def finalize_options(self):
        super().finalize_options()
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        # import here, so the egg files have been loaded
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)


setup(
    name='fandango',
    version='0.1',
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Pyramid',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
    ],
    url='',
    license='AGPL-3.0-or-later',
    keywords='web pyramid pylons',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=True,
    extras_require={
        'testing': tests_require,
    },
    install_requires=requires,
    setup_requires=setup_requires,
    cmdclass={'test': PyTest},
    entry_points={
        'paste.app_factory': [
            'main = fandango:main',
        ],
    },
)
