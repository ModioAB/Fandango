Fandango
===============================

Fandango: a small web based PDF interface, including offline script
---------------

Pyramid/Deform based python program for adding names and locations to certain PDFs  

This will be used for stuff like contracts, certificates etc.  
The main goal is for this to be a user friendly web interface for customers to 
fill out PDF forms, sign, send em to us for signing (if necessary) and then
ship them out to Energimyndigheten, or whoever should have them.

Milestones:
---------------

- reached
    *  [x] Package for 600 dpi image generation from PDFs
    *  [x] Image append: program that takes an empty PDF and inserts images into it
    *  [x] Text adder: program that adds texts to .PNGs
    *  [x] Pdfinsert script
    *  [x] Implement package into Pyramid deform project - text input frontend

- goals
    *  [x] Dockerise
    *  [x] Make sure temporary files aren't kept
    *  [x] Write view test cases!
    *  [x] Don't insert 'None's, handle empty fields
    *  [ ] Cut run time considerably
        *  TIMED RUNS, form/PDF 2:
        *  initially: ~120 sec
        *  2018-02-14: ~13.7 sec
        *  Goal: ~5 sec
    *  [ ] Validation of forms
    *  [x] Give the output PDF a (better) filename in browser
    *  [ ] User friendliness, explanations
    *  [x] Some sort of preview functionality
    *  [x] Maintain visible field names even after failed input
    *  [x] Cap pages in config to stay within page bounds of PDF

- design ideas
    *  [ ] BETTER preview functionality
    *  [x] Fix multi-lines to linebreak after appropriate num. of chars
    *  [x] Checkboxes for checkbox things ("ja", "nej")
    *  [x] Date inputs
    *  [ ] Date input constraints?
    *  [x] Some fields might "need" to be required
    *  [x] Only regenerate the pdf if the form contains something
    *  [x] Fix field duplicates..
    *  [x] Boil down "config_builder"/"config_parser" to one function



Instructions:
---------------

- cd into `Fandango`

- Create a virtual environment  
    `python3 -m venv env`

- Activate virtual environment  
    `source env/bin/activate`


- Web service:  
    - install pyramid:  
        `env/bin/pip install "pyramid==1.9.1" waitress`  
        `env/bin/pip install pytest deform pyramid_jinja2`  
        `env/bin/pip install -e .`  

    - set local prefilling placeholders  
        `export PREFILL=`cat fandango/bonus.json``

    - run tests:  
        `env/bin/py.test --cov-report=html --cov=. fandango/tests/`

    - start web app:  
        `env/bin/pserve development.ini --reload`

    - visit `localhost:6543` in your browser
