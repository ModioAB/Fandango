FROM scratch

ARG URL=unknown
ARG COMMIT=unknown
ARG BRANCH=unknown
ARG HOST=unknown
ARG DATE=unknown

LABEL "se.modio.ci.url"=$URL \
 "se.modio.ci.branch"=$BRANCH \
 "se.modio.ci.commit"=$COMMIT \
 "se.modio.ci.host"=$HOST \
 "se.modio.ci.date"=$DATE

ENV LANG C.UTF-8
ENV LC_CTYPE C.utf8

ADD rootfs.tar /
ADD fandango.tar /

RUN cd /srv/webapp  && \
    pip3 --no-cache-dir install . && \
    echo fandango:x:110140:100:fandango:/srv/webapp:/sbin/nologin >> /etc/passwd

USER 110140
WORKDIR /srv/webapp
EXPOSE 6543

CMD /srv/webapp/launcher
